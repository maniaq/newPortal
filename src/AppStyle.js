import styled from 'styled-components'


export const NewsContainer = styled.div`
    overflow: hidden;
    marginTop: 16px;
    width: 1100px;
    margin: 16px auto;
    border: 1px solid;
`;

export const PictureNews = styled.div`
    float: left;
    width: 320px;
    height: 320px;
    box-shadow: 0 0 20px rgba(99, 99, 99, .5);
    margin: 16px 8px 16px 16px;
    background:white;
`;

export const HeadNews = styled.div`
    width: 480px;
    height: 320px;
    background: green;
    float: left;
    margin: 16px 8px 8px 8px;
    box-shadow: 0 0 20px  rgba(99, 99, 99, .5);
`;

export const QuickFunc = styled.div`
    width: 228px;
    height: 240px;
    background: green;
    box-shadow: 0 0 20px  rgba(99, 99, 99, .5);
    &.Left {
        margin: 16px 8px 8px 
        float: left;
    }
    &.Right {
        margin-right: 24px;
        margin-top: 8px;
        float: right;
    }
`;

export const MiddleBar = styled.div`
    width: 816px;
    height: 160px;
    float: left;
    top: 0;
    margin-bottom: 16px;
    margin-left: 16px ;
    background: red;
    box-shadow: 0 0 20px  rgba(99, 99, 99, .5);

`

export const SecondNews = styled.div`
    width: 345px;
    height: 400px;
    float: left;
    margin-left: 16px;
    background: blue;
    
`