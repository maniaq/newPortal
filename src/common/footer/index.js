import React, { Component } from 'react';
import { FooterWrapper, FooterAbout } from './style';


class Footer extends Component {
	render() {
		return (
			// 底部考虑四栏布局，第一栏以联盟简介为主，第二栏新闻、关于， 第三栏主要产品，第四栏联系我、合作伙伴、加入我
			<FooterWrapper>
				<FooterAbout>
				
				山东省城市商业银行合作联盟有限公司
				</FooterAbout>
			</FooterWrapper>
		)
	}
}

export default Footer;