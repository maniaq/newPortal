import styled from 'styled-components';


export const FooterWrapper = styled.div`
    width: 1100px;
    height: 239px;
    background: #002B69;
    padding-top: 10px;
    margin: 16px auto;
    color: white;
`;

export const FooterAbout = styled.div`
    width: 259px;
    margin: 32px 32px;
    text-align: left;
    font-size: 16px;
    font-weight: 400;
    font-family: 黑体;
`