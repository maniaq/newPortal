import React, { Component } from 'react';
import PrimarySearchAppBar from './common/header'
import { 
  NewsContainer, 
  PictureNews, 
  HeadNews, 
  QuickFunc, 
  MiddleBar, 
  SecondNews
} from './AppStyle.js'
import Footer from './common/footer';
import Slider from 'react-slick';
import image1 from './static/1.jpg';
import image2 from './static/2.jpg';
import image3 from './static/3.jpg'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import store from './store';

class App extends Component {

  constructor(props) {
    super(props)
    console.log(store.getState())
    this.state = store.getState();
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      adaptiveHeight: true,
      arrows:false,
    };
    return (
      <div className="Container">
        <PrimarySearchAppBar></PrimarySearchAppBar>
        <NewsContainer>
          <PictureNews>
          <Slider {...settings} >
                  <div>
                    <img
                      src={image1}
                      alt="First slide"
                      className="slick-image"
                      style={{width:"320px", height:"240px"}}
                    />
                    <div className="slick-caption">
                      <h4 style={{ margin:"auto 16px"}}>
                       Yellowstone
                        National Park, United States
                      </h4>
                    </div>
                  </div>
                  <div>
                    <img
                      src={image2}
                      alt="Second slide"
                      className="slick-image"
                      style={{width:"320px", height:"240px"}}
                    />
                    <div className="slick-caption">
                      <h4 style={{ margin:"auto 16px"}}>
                        Somewhere Beyond,
                        United States
                      </h4>
                    </div>
                  </div>
                  <div>
                    <img
                      src={image3}
                      alt="Third slide"
                      className="slick-image"
                      style={{width:"320px", height:"240px"}}
                    />
                    <div className="slick-caption">
                      <h4 style={{ margin:"auto 16px"}}>
                        Yellowstone
                        National Park, United States
                      </h4>
                    </div>
                  </div>
                </Slider>  
          </PictureNews> 
          <HeadNews>新闻主题</HeadNews>

        <QuickFunc className="Left">快捷菜单</QuickFunc>
        <QuickFunc className="Right">快捷菜单二 带图片按钮</QuickFunc>
        <MiddleBar>中间照片</MiddleBar>
        <SecondNews>1号</SecondNews>
        <SecondNews>2号</SecondNews>
        <SecondNews>3号</SecondNews>

        </NewsContainer>
        <Footer></Footer>
      </div>
    );
  }
}

export default App;
